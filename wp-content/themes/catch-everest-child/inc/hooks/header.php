<?php 

if ( ! function_exists( 'catcheverest_header_logo' ) ) :
/**
 * Shows Header top content
 *
 * Shows the site logo, title and description
 * @uses header action to add it in the header
 */
function catcheverest_header_logo() { ?>

	<div class="container">
        <div class="row">
        	<?php
        	if ( function_exists( 'has_custom_logo' ) ) {
				if ( has_custom_logo() ) { ?>
		            <div class="col-md-12 text-center site-logo-col">
		                <?php the_custom_logo();?>
		            </div>
		        <?php
				} else { ?>
					<div class="col-md-12 text-center">
						<h1 id="site-title" style="position: relative !important;"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1>
						<?php

						$description = get_bloginfo( 'description', 'display' );
						if ( $description || is_customize_preview() ) : ?>
							<p id="site-description" style="position: relative !important; padding-bottom: 10px;"><?php echo $description; ?></p>
						<?php endif; ?>
					</div>
				<?php
				} 
			} ?>
        </div>
    </div>

<?php
}
endif; // catcheverest_header_logo

add_action( 'catcheverest_hgroup_wrap', 'catcheverest_header_logo', 10 );

if ( ! function_exists( 'catcheverest_header_social' ) ) :
/**
 * Shows Header Left content
 *
 * Shows the site logo, title and description
 * @uses header action to add it in the header
 */
function catcheverest_header_social() { ?>

	<div class="container">
        <div class="row">
            <div class="col-md-12 social-nav-text-center">
                <?php catcheverest_social_networks();?>
            </div>
        </div>
    </div>
<?php
}

endif; // catcheverest_header_logo
add_action( 'catcheverest_hgroup_wrap', 'catcheverest_header_social', 15 );

/**
 *
 * Shows the Cart Menu
 */
function catcheverest_header_cart_menu() { 
    ?>
    <div id="cart-container" class="container">
        <?php
            wp_nav_menu( array(
                'theme_location' => 'cart',
                'menu_id'        => 'cart-menu',
                'menu_class'     => 'cart-menu',
            ) );
        ?>
    </div>
<?php

}
add_action( 'catcheverest_hgroup_wrap', 'catcheverest_header_cart_menu', 16 );

/**
 * Shows header right content
 *
 * Shows the Primary Menu
 * @uses catcheverest_header action to add it in the header
 */
function catcheverest_header_main_menu() { 
    if ( function_exists('max_mega_menu_is_enabled') && max_mega_menu_is_enabled('primary') ) {
    ?>
        <div id="main-menu-container">
            <?php
                wp_nav_menu( array(
                    'theme_location' => 'primary',
                    'menu_id'        => 'primary-menu',
                    'menu_class'     => 'primary-menu',
                ) );
            ?>
        </div>
    <?php
    } else {
    ?>
        <div id="main-menu-container" class="container">
            <div id="cssmenu">
                <?php
                wp_nav_menu( array(
                    'theme_location' => 'primary',
                    'menu_id'        => 'primary-menu',
                    'menu_class'     => 'primary-menu',
                ) );
                ?>
                <?php
                    wp_nav_menu( array(
                        'theme_location' => 'language',
                        'menu_id'        => 'language-menu',
                        'menu_class'     => 'language-menu',
                    ) );
                ?>
            </div>
        </div>
    <?php
    }
}
add_action( 'catcheverest_after_hgroup_wrap', 'catcheverest_header_main_menu', 10 );