<?php
// corrected container class name

/**
 * Function to pass the slider effectr parameters from php file to js file.
 */
function catcheverest_tweaked_pass_slider_value() {
    global $catcheverest_options_settings;
    $options = $catcheverest_options_settings;

    $transition_effect = $options['transition_effect'];
    $transition_delay = $options['transition_delay'] * 1000;
    $transition_duration = $options['transition_duration'] * 1000;
    wp_localize_script(
        'catcheverest-slider',
        'js_value',
        array(
            'transition_effect' => $transition_effect,
            'transition_delay' => $transition_delay,
            'transition_duration' => $transition_duration
        )
    );
}// catcheverest_tweaked_pass_slider_value

if ( ! function_exists( 'catcheverest_post_sliders' ) ) :
/**
 * Shows Featured Post Slider
 *
 * @uses catcheverest_header action to add it in the header
 */
function catcheverest_post_sliders() {
    delete_transient( 'catcheverest_post_sliders' );

    global $post, $catcheverest_options_settings;
    $options = $catcheverest_options_settings;


    if ( ( !$catcheverest_post_sliders = get_transient( 'catcheverest_post_sliders' ) ) && !empty( $options['featured_slider'] ) ) {

        $catcheverest_post_sliders = '
        <div id="main-slider" class="ced-container">
            <section class="featured-slider">';
                $loop = new WP_Query( array(
                    'posts_per_page' => $options['slider_qty'],
                    'post__in'       => $options['featured_slider'],
                    'orderby'        => 'post__in',
                    'ignore_sticky_posts' => 1 // ignore sticky posts
                ));
                $i=0; while ( $loop->have_posts()) : $loop->the_post(); $i++;
                    $title_attribute = the_title_attribute( 'echo=0' );
                    $excerpt = get_the_excerpt();
                    if ( $i == 1 ) { $classes = "post hentry slides displayblock"; } else { $classes = "post hentry slides displaynone"; }
                    $catcheverest_post_sliders .= '
                    <article class="'.$classes.'">
                        <figure class="slider-image">
                            <a title="' . $title_attribute . '" href="' . esc_url( get_permalink() ) . '">
                                '. get_the_post_thumbnail( $post->ID, 'slider', array( 'title' => $title_attribute, 'alt' => $title_attribute, 'class'  => 'pngfix' ) ).'
                            </a>
                        </figure>
                        <div class="entry-container">
                            <header class="entry-header">
                                <h2 class="entry-title">
                                    <a title="' . $title_attribute . '" href="' . esc_url( get_permalink() ) . '">'.the_title( '<span>','</span>', false ).'</a>
                                </h2>
                            </header>';
                            if ( $excerpt !='') {
                                $catcheverest_post_sliders .= '<div class="entry-content">'. $excerpt.'</div>';
                            }
                            $catcheverest_post_sliders .= '
                        </div>
                    </article><!-- .slides -->';
                endwhile; wp_reset_postdata();
                $catcheverest_post_sliders .= '
            </section>
            <div id="slider-nav">
                <a class="slide-previous">&lt;</a>
                <a class="slide-next">&gt;</a>
            </div>
            <div id="controllers"></div>
        </div><!-- #main-slider -->';

    set_transient( 'catcheverest_post_sliders', $catcheverest_post_sliders, 86940 );
    }
    echo $catcheverest_post_sliders;
}
endif; // catcheverest_post_sliders

/**
 * Shows Default Slider Demo if there is not iteam in Featured Post Slider
 */
function catcheverest_tweaked_default_sliders() {
    delete_transient( 'catcheverest_tweaked_default_sliders' );

    if ( !$catcheverest_tweaked_default_sliders = get_transient( 'catcheverest_tweaked_default_sliders' ) ) {
        $catcheverest_tweaked_default_sliders = '
        <div id="main-slider" class="ced-container">
            <section class="featured-slider">
                <article class="post hentry slides displayblock">
                    <figure class="slider-image">
                        <a title="Mount Everest" href="#">
                            <img src="'. trailingslashit( esc_url ( get_template_directory_uri() ) ) . 'images/slider1.jpg" class="wp-post-image" alt="Mount Everest" title="Mount Everest">
                        </a>
                    </figure>
                    <div class="entry-container">
                        <header class="entry-header">
                            <h2 class="entry-title">
                                <a title="Mount Everest" href="#"><span>Mount Everest</span></a>
                            </h2>
                        </header>
                        <div class="entry-content">
                            <p>Mount Everest is the Earth\'s highest mountain, with a peak at 8,848 metres above sea level and the 5th tallest mountain measured from the centre of the Earth. It is located in the Nepal.</p>
                        </div>
                    </div>
                </article><!-- .slides -->

                <article class="post hentry slides displaynone">
                    <figure class="slider-image">
                        <a title="Nepal Prayer Wheels" href="#">
                            <img src="'. trailingslashit( esc_url ( get_template_directory_uri() ) ) . 'images/slider2.jpg" class="wp-post-image" alt="Nepal Prayer Wheels" title="Nepal Prayer Wheels">
                        </a>
                    </figure>
                    <div class="entry-container">
                        <header class="entry-header">
                            <h2 class="entry-title">
                                <a title="Nepal Prayer Wheels" href="#"><span>Nepal Prayer Wheels</span></a>
                            </h2>
                        </header>
                        <div class="entry-content">
                            <p>A prayer wheel is a cylindrical wheel on a spindle made from metal, wood, stone, leather or coarse cotton. Traditionally, the mantra Om Mani Padme Hum is written in Sanskrit on the outside of the wheel.</p>
                        </div>
                    </div>
                </article><!-- .slides -->
            </section>
            <div id="slider-nav">
                <a class="slide-previous">&lt;</a>
                <a class="slide-next">&gt;</a>
            </div>
            <div id="controllers"></div>
        </div><!-- #main-slider -->';

    set_transient( 'catcheverest_tweaked_default_sliders', $catcheverest_tweaked_default_sliders, 86940 );
    }
    echo $catcheverest_tweaked_default_sliders;
} // catcheverest_tweaked_default_sliders

if ( ! function_exists( 'catcheverest_slider_display' ) ) :
/**
 * Shows Slider
 */
function catcheverest_slider_display() {
    global $post, $wp_query, $catcheverest_options_settings;
    $options = $catcheverest_options_settings;

    // get data value from theme options
    $enableslider = $options['enable_slider'];
    $featuredslider = $options['featured_slider'];

    // Front page displays in Reading Settings
    $page_on_front = get_option('page_on_front') ;
    $page_for_posts = get_option('page_for_posts');

    // Get Page ID outside Loop
    $page_id = $wp_query->get_queried_object_id();

    if ( ( 'enable-slider-allpage' == $enableslider ) || ( ( is_front_page() || ( is_home() && $page_for_posts != $page_id ) ) && 'enable-slider-homepage' == $enableslider ) ) :


        // This function passes the value of slider effect to js file
        if ( function_exists( 'catcheverest_tweaked_pass_slider_value' ) ) : catcheverest_tweaked_pass_slider_value(); endif;

        // Select Slider
        if ( !empty( $featuredslider ) ) {
            catcheverest_post_sliders();
        }
        else {
            catcheverest_tweaked_default_sliders();
        }


    endif;
}
endif; // catcheverest_slider_display

add_action( 'catcheverest_before_main', 'catcheverest_slider_display', 10 );


if ( ! function_exists( 'catcheverest_homepage_headline' ) ) :
/**
 * Shows Homepage Headline Message
 *
 * @uses catcheverest_before_main action to add it in the header
 */
function catcheverest_homepage_headline() {
    delete_transient( 'catcheverest_homepage_headline' );

    global $post, $wp_query, $catcheverest_options_settings;;
    $options = $catcheverest_options_settings;

    // Getting data from Theme Options
    $disable_headline = $options['disable_homepage_headline'];
    $disable_subheadline = $options['disable_homepage_subheadline'];
    $homepage_headline = $options['homepage_headline'];
    $homepage_subheadline = $options['homepage_subheadline'];

    // Front page displays in Reading Settings
    $page_on_front = get_option('page_on_front') ;
    $page_for_posts = get_option('page_for_posts');

    // Get Page ID outside Loop
    $page_id = $wp_query->get_queried_object_id();


    if ( ( empty( $disable_headline ) || empty( $disable_subheadline ) ) && ( is_front_page() || ( is_home() && $page_for_posts != $page_id ) ) ) {
        $catcheverest_homepage_headline = '<div id="homepage-message" class="ced-container"><p>';

        if ( $disable_headline == "0" ) {
            if (function_exists('pll__')) {
                $catcheverest_homepage_headline .= pll__('Homepage Headline');
            } else {
                $catcheverest_homepage_headline .= $homepage_headline;
            } 
        }
        if ( $disable_subheadline == "0" ) {
            if (function_exists('pll__')) {
                $catcheverest_homepage_headline .= '<span>' . pll__('Homepage Sub-Headline') . '</span>';
            } else {
                $catcheverest_homepage_headline .= '<span>' . $homepage_subheadline . '</span>';
            } 
        }

        $catcheverest_homepage_headline .= '</p></div>';

        echo $catcheverest_homepage_headline;
    }
    

}
endif; // catcheverest_homepage_headline

add_action( 'catcheverest_before_main', 'catcheverest_homepage_headline', 15 );

/**
 * Shows Default Featued Content
 *
 * @uses catcheverest_before_main action to add it in the header
 */
function catcheverest_child_default_featured_content() {
    delete_transient( 'catcheverest_default_featured_content' );

    // Getting data from Theme Options
    global $catcheverest_options_settings;
    $options = $catcheverest_options_settings;
    $disable_homepage_featured = $options['disable_homepage_featured'];

    if ( $disable_homepage_featured == "0" ) {
        if ( !$catcheverest_default_featured_content = get_transient( 'catcheverest_default_featured_content' ) ) {

            $classes = "layout-three";

            $catcheverest_default_featured_content = '
            <section id="featured-post" class="' . $classes . '">
                <article class="post hentry first">
                    <figure class="featured-homepage-image">
                        <a href="#" title="Nepal Prayer Wheels">
                            <img title="Nepal Prayer Wheels" alt="Nepal Prayer Wheels" class="wp-post-image" src="'.trailingslashit( esc_url ( get_template_directory_uri() ) ) . 'images/thumb-390-1.jpg" />
                        </a>
                    </figure>
                    <div class="entry-container">
                        <header class="entry-header">
                            <h2 class="entry-title">
                                <a title="Nepal Prayer Wheels" href="#">Nepal Prayer Wheels</a>
                            </h2>
                        </header>
                        <div class="entry-content">
                            A prayer wheel is a cylindrical wheel on a spindle made from metal, wood, stone, leather or coarse cotton. Traditionally, the mantra Om Mani Padme Hum is written in Sanskrit on the outside of the wheel.
                        </div>
                    </div><!-- .entry-container -->
                </article>

                <article class="post hentry">
                    <figure class="featured-homepage-image">
                        <a href="#" title="Mount Everest">
                            <img title="Mount Everest" alt="Mount Everest" class="wp-post-image" src="'.trailingslashit( esc_url ( get_template_directory_uri() ) ) . 'images/thumb-390-2.jpg" />
                        </a>
                    </figure>
                    <div class="entry-container">
                        <header class="entry-header">
                            <h2 class="entry-title">
                                <a title="Mount Everest" href="#">Mount Everest</a>
                            </h2>
                        </header>
                        <div class="entry-content">
                            Mount Everest is the Earth\'s highest mountain, with a peak at 8,848 metres above sea level and the 5th tallest mountain measured from the centre of the Earth. It is located in the Nepal.
                        </div>
                    </div><!-- .entry-container -->
                </article>

                <article class="post hentry">
                    <figure class="featured-homepage-image">
                        <a href="#" title="Mount Kanchengjunga">
                            <img title="Mount Kanchengjunga" alt="Mount Kanchengjunga" class="wp-post-image" src="'.trailingslashit( esc_url ( get_template_directory_uri() ) ) . 'images/thumb-390-3.jpg" />
                        </a>
                    </figure>
                    <div class="entry-container">
                        <header class="entry-header">
                            <h2 class="entry-title">
                                <a title="Mount Kanchengjunga" href="#">Mount Kanchengjunga</a>
                            </h2>
                        </header>
                        <div class="entry-content">
                            Kangchenjunga is the third highest mountain in the world, with apeat at 8,586 metres above sea level. It is located on the boundary between Nepal and the Indian state of Sikkim.
                        </div>
                    </div><!-- .entry-container -->
                </article>
            </section><!-- #featured-post -->';
        }
        echo $catcheverest_default_featured_content;
    }
}

if ( ! function_exists( 'catcheverest_homepage_featured_content' ) ) :
/**
 * Homepage Featured Content
 *
 * @uses catcheverest_before_main action to add it in the header
 */
function catcheverest_homepage_featured_content() {
    delete_transient( 'catcheverest_homepage_featured_content' );

    // Getting data from Theme Options
    global $catcheverest_options_settings;
    $options = $catcheverest_options_settings;
    $disable_homepage_featured = $options['disable_homepage_featured'];
    $quantity = $options['homepage_featured_qty'];
    $headline = $options['homepage_featured_headline'];

    if ( $disable_homepage_featured == "0" ) {

        if ( !$catcheverest_homepage_featured_content = get_transient( 'catcheverest_homepage_featured_content' )  && ( !empty( $options['homepage_featured_image'] ) || !empty( $options['homepage_featured_title'] ) || !empty( $options['homepage_featured_content'] ) ) ) {

            $parentclasses = "layout-three";

            $catcheverest_homepage_featured_content = '<section id="featured-post" class="' . $parentclasses . '">';

            if ( !empty( $headline ) ) {
                $catcheverest_homepage_featured_content .= '<h2 id="feature-heading" class="entry-title">' . wp_kses_post( $headline ) . '</h2>';
            }

            $catcheverest_homepage_featured_content .= '<div class="featued-content-wrap">';

                for ( $i = 1; $i <= $quantity; $i++ ) {


                    if ( !empty ( $options['homepage_featured_base'][ $i ] ) ) {
                        $target = '_blank';
                    } else {
                        $target = '_self';
                    }

                    //Adding in Classes for Display blok and none
                    if ( $i % 3 == 1  || $i == 1 ) {
                        $classes = "post hentry first";
                    }
                    else {
                        $classes = "post hentry";
                    }

                    //Checking Link
                    if ( !empty ( $options['homepage_featured_url'][ $i ] ) ) {
                        if (function_exists('pll__')) {
                            $link = pll__('Homepage Featured URL ' . $i);
                        } else {
                            $link = $options['homepage_featured_url'][ $i ];
                        } 
                    } else {
                        $link = '#';
                    }

                    //Checking Title
                    if ( !empty ( $options['homepage_featured_title'][ $i ] ) ) {
                        if (function_exists('pll__')) {
                            $title = pll__('Homepage Featured Title ' . $i);
                        } else {
                            $title = $options['homepage_featured_title'][ $i ];
                        } 
                    } else {
                        $title = '';
                    }


                    if ( !empty ( $options['homepage_featured_title'][ $i ] ) || !empty ( $options['homepage_featured_content'][ $i ] ) || !empty ( $options['homepage_featured_image'][ $i ] ) ) {
                        $catcheverest_homepage_featured_content .= '
                        <article class="'.$classes.'">';
                            if ( !empty ( $options['homepage_featured_image'][ $i ] ) ) {
                                $catcheverest_homepage_featured_content .= '
                                <figure class="featured-homepage-image">
                                    <a title="' . esc_attr( $title ) . '" href="' . esc_url( $link ) . '" target="' . $target . '">
                                        <img src="'.$options['homepage_featured_image'][ $i ].'" class="wp-post-image" alt="' . esc_attr( $title ) . '" title="' . esc_attr( $title ) . '">
                                    </a>
                                </figure>';
                            }
                            if ( !empty ( $options['homepage_featured_title'][ $i ] ) || !empty ( $options['homepage_featured_content'][ $i ] ) ) {
                                $catcheverest_homepage_featured_content .= '
                                <div class="entry-container">';

                                    if ( !empty ( $options['homepage_featured_title'][ $i ] ) ) {
                                        $catcheverest_homepage_featured_content .= '
                                        <header class="entry-header">
                                            <h2 class="entry-title">
                                                <a href="' . esc_url( $link ) . '" title="' . esc_attr( $title ) . '" target="' . $target . '">' . $title . '</a>
                                            </h2>
                                        </header>';
                                    }
                                    if ( !empty ( $options['homepage_featured_content'][ $i ] ) ) {
                                        if (function_exists('pll__')) {
                                            $catcheverest_homepage_featured_content .= '
                                            <div class="entry-content">
                                                ' . pll__('Homepage Featured Content ' . $i) . '
                                            </div>';
                                        } else {
                                            $catcheverest_homepage_featured_content .= '
                                            <div class="entry-content">
                                                ' . $options['homepage_featured_content'][ $i ] . '
                                            </div>';
                                        } 
                                    }
                                $catcheverest_homepage_featured_content .= '
                                </div><!-- .entry-container -->';
                            }
                        $catcheverest_homepage_featured_content .= '
                        </article><!-- .slides -->';
                    }

                }

            $catcheverest_homepage_featured_content .= '</div><!-- .featued-content-wrap -->';

            $catcheverest_homepage_featured_content .= '</section><!-- #featured-post -->';

        }

        echo $catcheverest_homepage_featured_content;

    }

}
endif; // catcheverest_homepage_featured_content


/**
 * Homepage Featured Content
 *
 * @uses catcheverest_before_main action to add it in the header
 */
function catcheverest_child_homepage_featured_display() {

    global $post, $wp_query, $catcheverest_options_settings;;
    $options = $catcheverest_options_settings;

    // Getting data from Theme Options
    $options = $catcheverest_options_settings;
    $disable_homepage_featured = $options['disable_homepage_featured'];

    // Front page displays in Reading Settings
    $page_on_front = get_option('page_on_front') ;
    $page_for_posts = get_option('page_for_posts');

    // Get Page ID outside Loop
    $page_id = $wp_query->get_queried_object_id();


    if ( is_front_page() || ( is_home() && $page_for_posts != $page_id ) ) {
        if ( ( !empty( $options['homepage_featured_image'] ) && ( array_filter( $options['homepage_featured_image'] ) ) ) || ( !empty( $options['homepage_featured_title'] ) && ( array_filter( $options['homepage_featured_title'] ) ) ) || ( !empty( $options['homepage_featured_content'] ) && ( array_filter( $options['homepage_featured_content'] ) ) ) ) {
            catcheverest_homepage_featured_content();
        } else {
            catcheverest_child_default_featured_content();
        }
    }

} // catcheverest_homepage_featured_content

add_action( 'catcheverest_main', 'catcheverest_child_homepage_featured_display', 10 );



/**
 * Function to display copyright info
 *
 * @return string
 */
function copyright_info() {
    $catcheverest_content = '';
    if (function_exists('pll__')) {
        $catcheverest_content = '<div class="copyright-text">'. pll__('Copyright') . ' &copy; '. catcheverest_the_year() . ' ' . catcheverest_site_link() . ' ' . pll__('All Rights Reserved') . '</div>';
    } else {
        $catcheverest_content = '<div class="copyright-text">'. esc_attr__( 'Copyright', 'catch-everest' ) . ' &copy; '. catcheverest_the_year() . ' ' . catcheverest_site_link() . ' ' . esc_attr__( 'All Rights Reserved', 'catch-everest' ) . '</div>';
    }
    return $catcheverest_content;
}


/**
 * Function for footer content
 */
function lovexpress_footer_copyright_content() { ?>

    <div class="container-footer">
        <div class="row">
            <div class="col-lg-12">
    <?php
    $catcheverest_footer_content = copyright_info();

    echo $catcheverest_footer_content;
    ?>
            </div>
        </div>
    </div>
<?php
}
add_action( 'catcheverest_before_site_info', 'lovexpress_footer_copyright_content', 30 );

function lovexpress_footer_menu() { ?>
    <div class="footer-menu">
        <?php
            wp_nav_menu( array(
                'theme_location' => 'footer',
                'menu_id'        => 'footer-bottom',
                'menu_class'     => 'footer-bottom',
            ) );
            // wp_nav_menu( array(
            //     'theme_location' => 'footer',
            //     'depth' => 2,
            //     'container' => false,
            //     'menu_class'     => 'nav navbar-nav',
            //     'fallback_cb' => 'WP_Bootstrap_Navwalker::fallback',
            //     'walker' => new WP_Bootstrap_Navwalker(),
            // ) );
        ?>
    </div>
<?php
}

add_action( 'catcheverest_before_site_info', 'lovexpress_footer_menu', 20 );


/**
 * Removes div from wp_page_menu() and replace with ul.
 *
 * @since Catch Everest 1.0
 */
function catcheverest_tweaked_wp_page_menu ($page_markup) {
    $divclass = '';
    if (preg_match('/^<div class=\"([a-z0-9-_]+)\">/i', $page_markup, $matches)) {
        $divclass = $matches[1];
    }
    $replace = array('<div class="'.$divclass.'">', '</div>');
    $new_markup = str_replace($replace, '', $page_markup);
    $new_markup = preg_replace('/^<ul>/i', '<ul class="'.$divclass.'">', $new_markup);
    return $new_markup; 
}

add_filter('wp_page_menu', 'catcheverest_tweaked_wp_page_menu');


/**
 * Returns a "Continue Reading" link for excerpts
 */
function catcheverest_tweaked_continue_reading() {
    $more_tag_text = '';

    if (function_exists('pll__')) {
        $more_tag_text .= pll__('Continue Reading &rarr;');
    } else {
            // Getting data from Theme Options
        global $catcheverest_options_settings;
        $options = $catcheverest_options_settings;

        $more_tag_text = $options['more_tag_text'];
    }

    return ' <a class="more-link" href="'. esc_url( get_permalink() ) . '">' .  $more_tag_text . '</a>';
}

/**
 * Replaces "[...]" (appended to automatically generated excerpts) with catcheverest_tweaked_continue_reading().
 *
 */
function catcheverest_tweaked_excerpt_more( $more ) {
    return catcheverest_tweaked_continue_reading();
}
add_filter( 'excerpt_more', 'catcheverest_tweaked_excerpt_more' );

/**
 * Adds Continue Reading link to post excerpts.
 *
 * function tied to the get_the_excerpt filter hook.
 */
function catcheverest_tweaked_custom_excerpt( $output ) {
    if ( has_excerpt() && ! is_attachment() ) {
        $output .= catcheverest_tweaked_continue_reading();
    }
    return $output;
}
add_filter( 'get_the_excerpt', 'catcheverest_tweaked_custom_excerpt' );


/**
 * Replacing Continue Reading link to post content more.
 *
 * function tied to the the_content_more_link filter hook.
 */
function catcheverest_tweaked_more_link( $more_link, $more_link_text ) {
    $more_tag_text = '';

    if (function_exists('pll__')) {
        $more_tag_text .= pll__('Continue Reading &rarr;');
    } else {
            // Getting data from Theme Options
        global $catcheverest_options_settings;
        $options = $catcheverest_options_settings;

        $more_tag_text = $options['more_tag_text'];
    }

    return str_replace( $more_link_text, $more_tag_text, $more_link );
}
add_filter( 'the_content_more_link', 'catcheverest_tweaked_more_link', 10, 2 );

?>
