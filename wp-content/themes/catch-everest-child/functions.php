<?php
function my_theme_enqueue_styles() {

    $parent_style = 'catcheverest-style'; 

    wp_enqueue_style( 'lato-font', 'https://fonts.googleapis.com/css?family=Lato', false);
    wp_enqueue_style( 'font-awesome', 'https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css');

    wp_enqueue_style( $parent_style, get_template_directory_uri() . '/style.css' );
    wp_enqueue_style( 'catcheverest-child-style',
        get_stylesheet_directory_uri() . '/style.css',
        array( $parent_style ),
        wp_get_theme()->get('Version')
    );

    wp_enqueue_script( 'jquery', get_stylesheet_directory_uri() . '/assets/bootstrap/js/jquery-3.3.1.min.js' );
    wp_enqueue_script( 'menu-js', get_stylesheet_directory_uri() . '/assets/js/menu.js' );
}
add_action( 'wp_enqueue_scripts', 'my_theme_enqueue_styles' );

function register_multiple_menus() {
    register_nav_menus( array(
        'language' => esc_html__( 'Language', 'lovexpress' ),
        'footer' => esc_html__( 'Footer Menu', 'lovexpress' ),
        'cart' => esc_html__( 'Cart', 'lovexpress' ),
    ) );
}
add_action( 'init', 'register_multiple_menus' ); 

if (function_exists('pll_register_string')) {
    pll_register_string('Menu Collapsed', 'Menu');
    pll_register_string('Homepage Headline', 'Homepage Headline');
    pll_register_string('Homepage Sub-Headline', 'Homepage Sub-Headline');
    pll_register_string('Homepage Featured Title 1', 'Homepage Featured Title 1');
    pll_register_string('Homepage Featured Title 2', 'Homepage Featured Title 2');
    pll_register_string('Homepage Featured Title 3', 'Homepage Featured Title 3');
    pll_register_string('Homepage Featured URL 1', 'Homepage Featured URL 1');
    pll_register_string('Homepage Featured URL 2', 'Homepage Featured URL 2');
    pll_register_string('Homepage Featured URL 3', 'Homepage Featured URL 3');
    pll_register_string('Homepage Featured Content 1', 'Homepage Featured Content 1');
    pll_register_string('Homepage Featured Content 2', 'Homepage Featured Content 2');
    pll_register_string('Homepage Featured Content 3', 'Homepage Featured Content 3');
    pll_register_string('Continue Reading', 'Continue Reading &rarr;');
    pll_register_string('Footer', 'Copyright');
    pll_register_string('Footer', 'All Rights Reserved');
}

// includes the file
include_once(dirname(__FILE__).'/inc/hooks/header.php');
include_once(dirname(__FILE__).'/inc/child-functions.php');

function remove_parent_action() {
    remove_action('catcheverest_hgroup_wrap', 'catcheverest_header_left', 10);
    remove_action('catcheverest_hgroup_wrap', 'catcheverest_header_right', 15);
    remove_action('catcheverest_before_main', 'catcheverest_homepage_headline', 10);
    remove_action('catcheverest_after_hgroup_wrap', 'catcheverest_header_menu', 10);
    remove_action('catcheverest_site_generator', 'catcheverest_footer_content', 10);
    remove_action( 'catcheverest_main', 'catcheverest_homepage_featured_display', 10 );
}
add_action('wp_head', 'remove_parent_action');

function remove_parent_filter() {
    remove_filter( 'excerpt_more', 'catcheverest_excerpt_more' );
    remove_filter( 'get_the_excerpt', 'catcheverest_custom_excerpt' );
    remove_filter( 'the_content_more_link', 'catcheverest_more_link', 10, 2 );
    remove_filter('wp_page_menu', 'catcheverest_wp_page_menu');
}
add_action('wp_head', 'remove_parent_filter');

function lovexpress_give_form_required_fields( $required_fields ) {

	//First Name
    $required_fields['give_first'] = array(
        'error_id' => 'invalid_last_name',
        'error_message' => __( 'Please enter your first name.', 'give' )
    );

	//Last Name
    $required_fields['give_last'] = array(
        'error_id' => 'invalid_last_name',
        'error_message' => __( 'Please enter your last name.', 'give' )
    );

    return $required_fields;
}
add_filter( 'give_purchase_form_required_fields', 'lovexpress_give_form_required_fields' );

function my_custom_gateway_labels($gateways) {
        $gateways['offlinebank'] = array(
                'admin_label'    => 'Offline Bank',
                'checkout_label' => __( 'Bank Transfer (Deposit directly to HSBC : 582-351-375-001)', 'give' )
        );
    return $gateways;
}

add_filter('give_payment_gateways', 'my_custom_gateway_labels');

add_action('give_offlinebank_cc_form', 'give_offline_payment_cc_form');

function give_offlinebank_process_payment( $purchase_data ) {
	// Setup the payment details.
	$payment_data = array(
		'price'           => $purchase_data['price'],
		'give_form_title' => $purchase_data['post_data']['give-form-title'],
		'give_form_id'    => intval( $purchase_data['post_data']['give-form-id'] ),
		'give_price_id'   => isset( $purchase_data['post_data']['give-price-id'] ) ? $purchase_data['post_data']['give-price-id'] : '',
		'date'            => $purchase_data['date'],
		'user_email'      => $purchase_data['user_email'],
		'purchase_key'    => $purchase_data['purchase_key'],
		'currency'        => give_get_currency( $purchase_data['post_data']['give-form-id'], $purchase_data ),
		'user_info'       => $purchase_data['user_info'],
		'status'          => 'pending',
		'gateway'         => 'offlinebank',
	);
	// record the pending payment
	$payment = give_insert_payment( $payment_data );
	if ( $payment ) {
		give_send_to_success_page();
	} else {
		// if errors are present, send the user back to the donation form so they can be corrected
		give_send_back_to_checkout( '?payment-mode=offline' );
	}
}
add_action( 'give_gateway_offlinebank', 'give_offlinebank_process_payment' );

/**
 * Add Custom Donation Form Fields
 *
 * @param $form_id
 */
function give_lovexpress_custom_form_fields( $form_id ) {
        if ( $form_id == 1415) {
        ?>
    <div id="give-phone-wrap">
        <label class="give-label" for="give-phone"><?php _e( 'Phone no.', 'give'); ?>
        </label>
        <input class="give-text-input" name="give_phone" id="give-phone"></input>
    </div>
    <div id="give-referral-wrap">
        <label class="give-label" for="give-referral"><?php _e( 'How did you get to know about us?', 'give' ); ?> <span
                    class="give-tooltip icon icon-question"
                    data-tooltip="<?php _e( 'Please take a second to tell us how you first heard about LoveXpress.', 'give' ) ?>"></span>
        </label>
        <textarea class="give-textarea" name="give_referral" id="give-referral"></textarea>
    </div>
        <?php
  }
}

add_action( 'give_donation_form_after_email', 'give_lovexpress_custom_form_fields', 10, 1 );


/**
 * Add Field to Payment Meta
 *
 * Store the custom field data  meta.
 *
 * @param $payment_id
 * @param $payment_data
 *
 * @return mixed
 */
function lovexpress_give_donations_save_custom_fields( $payment_id, $payment_data ) {
	if ( isset( $_POST['give_phone']) ) {
		$message = implode( "\n", array_map( 'sanitize_text_field', explode( "\n", $_POST['give_phone'] ) ) );
                give_update_meta( $payment_id, 'give_phone', $message );
	} 
	if ( isset( $_POST['give_referral'] ) ) {
		$message = implode( "\n", array_map( 'sanitize_text_field', explode( "\n", $_POST['give_referral'] ) ) );
		give_update_meta( $payment_id, 'give_referral', $message );
	}
}

add_action( 'give_insert_payment', 'lovexpress_give_donations_save_custom_fields', 10, 2 );

/**
 * Show Data in Payment Details
 *
 * Show the custom field(s) on the payment details page in wp-admin within its own metabox.
 *
 * @param int $payment_id
 */
function give_lovexpress_donation_details( $payment_id ) {

        // Bounce out if no data for this transaction.
        $give_phone = give_get_meta( $payment_id, 'give_phone', true );
        if ( $give_phone ) : ?>
        <div class="phone-data postbox">
            <h3 class="hndle"><?php _e( 'Phone No', 'give' ); ?></h3>
            <div class="inside">
                                <?php echo wpautop( $give_phone ); ?>
            </div>
        </div>
        <?php endif;

	// Bounce out if no data for this transaction.
	$give_referral = give_get_meta( $payment_id, 'give_referral', true );
	if ( $give_referral ) : ?>
        <div class="referral-data postbox">
            <h3 class="hndle"><?php _e( 'Referral Information', 'give' ); ?></h3>
            <div class="inside">
				<?php echo wpautop( $give_referral ); ?>
            </div>
        </div>
	<?php endif;
}

add_action( 'give_view_donation_details_billing_after', 'give_lovexpress_donation_details', 10, 2 );

?>
