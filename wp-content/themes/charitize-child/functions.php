<?php
function my_theme_enqueue_styles() {

    $parent_style = 'charitize-style'; 

	wp_enqueue_style( 'bootstrap-css', get_stylesheet_directory_uri() . '/assets/bootstrap/css/bootstrap.min.css' );

    wp_enqueue_style( $parent_style, get_template_directory_uri() . '/style.css' );
    wp_enqueue_style( 'charitize-child-style',
        get_stylesheet_directory_uri() . '/style.css',
        array( $parent_style ),
        wp_get_theme()->get('Version')
    );
    wp_enqueue_script( 'jquery', get_stylesheet_directory_uri() . '/assets/bootstrap/js/jquery-3.3.1.min.js' );
    wp_enqueue_script( 'bootstrap-js', get_stylesheet_directory_uri() . '/assets/bootstrap/js/bootstrap.min.js' );
}
add_action( 'wp_enqueue_scripts', 'my_theme_enqueue_styles' );

?>

<?php
// add_action('customize_register', 'theme_footer_customizer');
// function theme_footer_customizer($wp_customize){
//     //adding section in wordpress customizer   
//     $wp_customize->add_section('footer_settings_section', array(
//       'title'          => 'Home/Extra Section'

//     ));
//     //adding setting for footer logo
//     $wp_customize->add_setting('footer_logo');
//     $wp_customize->add_control(new WP_Customize_Upload_Control($wp_customize,'footer_logo',array(
//      'label'      => 'Footer Logo',
//      'section'    => 'footer_settings_section',
//      'settings'   => 'footer_logo',
//      ))); 
// }
?>

<?php
function register_language_menu() {
	register_nav_menus( array(
		'language' => esc_html__( 'Language', 'charitize-child' )
	) );
}
add_action( 'init', 'register_language_menu' );	
?>

<?php
function change_custom_logo_class($html) {
	$html = str_replace('custom-logo', 'custom-logo site-logo center-block img-responsive', $html);
	return $html;
}
add_filter( 'get_custom_logo', 'change_custom_logo_class');
?>

<?php
// includes the file
include_once(dirname(__FILE__).'/wp-bootstrap-navwalker.php');
include_once(dirname(__FILE__).'/inc/hooks/header.php');
include_once(dirname(__FILE__).'/inc/hooks/home-about-us-section.php');
include_once(dirname(__FILE__).'/inc/hooks/home-loving-stone-section.php');
include_once(dirname(__FILE__).'/inc/hooks/home-donate-section.php');
include_once(dirname(__FILE__).'/inc/hooks/home-volunteer-section.php');
//include_once(dirname(__FILE__).'/inc/customizer/home-volunteer/panel.php');
include_once(dirname(__FILE__).'/inc/customizer/customizer.php');
?>

