<?php
global $charitize_panels;
global $charitize_sections;
global $charitize_settings_controls;
global $charitize_repeated_settings_controls;
global $charitize_customizer_defaults;

/*defaults values about us options*/
$charitize_customizer_defaults['charitize-home-about-us-enable-tagline'] = 0;
$charitize_customizer_defaults['charitize-home-about-us-single-words'] = 40;
$charitize_customizer_defaults['charitize-home-about-us-button-text'] = __('Learn More', 'charitize-child');
$charitize_customizer_defaults['charitize-home-about-us-button-link'] = '';

/* Feature section Enable settings*/
$charitize_sections['charitize-about-us-section-settings'] =
    array(
        'priority'       => 30,
        'title'          => __( 'Section Settings', 'charitize' ),
        'panel'          => 'charitize-about-us-panel',
    );

/*Enable tagline*/
$charitize_settings_controls['charitize-home-about-us-enable-tagline'] =
    array(
        'setting' =>     array(
            'default'              => $charitize_customizer_defaults['charitize-home-about-us-enable-tagline']
        ),
        'control' => array(
            'label'                 =>  __( 'Enable Tagline in section', 'charitize' ),
            'description'           =>  __( 'Enable Tagline on checked', 'charitize' ),
            'section'               => 'charitize-about-us-section-settings',
            'type'                  => 'checkbox',
            'priority'              => 10,
            'active_callback'       => ''
        )
    );


/*String in max to be appear as description on about us*/
$charitize_settings_controls['charitize-home-about-us-single-words'] =
    array(
        'setting' =>     array(
            'default'              => $charitize_customizer_defaults['charitize-home-about-us-single-words']
        ),
        'control' => array(
            'label'                 =>  __( 'Number Of Words', 'charitize' ),
            'description'           =>  __( 'Give number of words you wish to be appear on home page about us section', 'charitize' ),
            'section'               => 'charitize-about-us-section-settings',
            'type'                  => 'number',
            'priority'              => 20,
            'active_callback'       => '',
            'input_attrs' => array( 'min' => 1, 'max' => 200),

        )
    );

/*About Us button text*/
$charitize_settings_controls['charitize-home-about-us-button-text'] =
array(
    'setting' =>     array(
        'default'              => $charitize_customizer_defaults['charitize-home-about-us-button-text']
    ),
    'control' => array(
        'label'                 =>  __( 'Button Text', 'charitize' ),
        'description'           =>  __( 'Input text for display on button', 'charitize' ),
        'section'               => 'charitize-about-us-section-settings',
        'type'                  => 'text',
        'priority'              => 60,
        'active_callback'       => ''
    )
);

/*About Us button url*/
$charitize_settings_controls['charitize-home-about-us-button-link'] =
array(
    'setting' =>     array(
        'default'              => $charitize_customizer_defaults['charitize-home-about-us-button-link']
    ),
    'control' => array(
        'label'                 =>  __( 'About Us Section Button URL', 'charitize' ),
        'description'           =>  __( 'Will redirect to the costume URL ', 'charitize' ),
        'section'               => 'charitize-about-us-section-settings',
        'type'                  => 'url',
        'priority'              => 70,
        'active_callback'       => ''
    )
);
