<?php
global $charitize_panels;
/*creating panel for About Us Section setting*/
$charitize_panels['charitize-about-us-panel'] =
    array(
        'title'          => __( 'Home/Front About Us Section', 'charitize' ),
        'priority'       => 201
    );


/*about us section enable control */
require(dirname(__FILE__).'/options.php');

/*about us selection settings */
require(dirname(__FILE__).'/settings.php');
