<?php
global $charitize_panels;
global $charitize_sections;
global $charitize_settings_controls;
global $charitize_repeated_settings_controls;
global $charitize_customizer_defaults;

/*defaults values about us options*/
$charitize_customizer_defaults['charitize-about-us-enable'] = 0;
$charitize_customizer_defaults['charitize-about-us-page'] = 0;

/* Feature section Enable settings*/
$charitize_sections['charitize-about-us-options'] =
    array(
        'priority'       => 10,
        'title'          => __( 'Section Options', 'charitize' ),
        'panel'          => 'charitize-about-us-panel',
    );

/*About Us section enable control*/
$charitize_settings_controls['charitize-about-us-enable'] =
    array(
        'setting' =>     array(
            'default'              => $charitize_customizer_defaults['charitize-about-us-enable']
        ),
        'control' => array(
            'label'                 =>  __( 'Enable About Us Section', 'charitize' ),
            'description'           =>  __( 'Enable "About Us Section" on checked', 'charitize' ),
            'section'               => 'charitize-about-us-options',
            'type'                  => 'checkbox',
            'priority'              => 10,
            'active_callback'       => ''
        )
    );


    /*creating setting control for charitize-about-us-page start*/
    $charitize_settings_controls['charitize-about-us-page'] =
        array(
                'setting' =>     array(
                    'default'              => $charitize_customizer_defaults['charitize-about-us-page'],
                    ),
                'control' => array(
                    'label'                 =>  __( 'Select Page For About Us Section', 'charitize' ),
                    'description'           => '',
                    'section'               => 'charitize-about-us-options',
                    'type'                  => 'dropdown-pages',
                    'priority'              => 20
                )
        );
