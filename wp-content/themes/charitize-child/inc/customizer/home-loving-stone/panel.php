<?php
global $charitize_panels;
/*creating panel for Loving Stone Section setting*/
$charitize_panels['charitize-loving-stone-panel'] =
    array(
        'title'          => __( 'Home/Front Loving Stone Section', 'charitize' ),
        'priority'       => 205
    );

/*Loving Stone section enable control */
require(dirname(__FILE__).'/options.php');
//require get_template_directory().'/inc/customizer/home-donate/options.php';

/*Loving Stone selection settings */
require(dirname(__FILE__).'/settings.php');
//require get_template_directory().'/inc/customizer/home-donate/settings.php';