<?php
global $charitize_panels;
global $charitize_sections;
global $charitize_settings_controls;
global $charitize_repeated_settings_controls;
global $charitize_customizer_defaults;

/*defaults values Loving Stone options*/
$charitize_customizer_defaults['charitize-home-loving-stone-single-words'] = 40;
$charitize_customizer_defaults['charitize-home-loving-stone-button-text'] = __('Purchase','charitize-child');
$charitize_customizer_defaults['charitize-home-loving-stone-button-link'] = '';
// $charitize_customizer_defaults['charitize-home-loving-stone-right-button-text'] = __('Buy Gift Box','charitize');
// $charitize_customizer_defaults['charitize-home-loving-stone-right-button-link'] = '';

/* Feature section Enable settings*/
$charitize_sections['charitize-loving-stone-section-settings'] =
    array(
        'priority'       => 30,
        'title'          => __( 'Section Settings', 'charitize' ),
        'panel'          => 'charitize-loving-stone-panel',
    );


/*String in max to be appear as description on Loving Stone*/
$charitize_settings_controls['charitize-home-loving-stone-single-words'] =
    array(
        'setting' =>     array(
            'default'              => $charitize_customizer_defaults['charitize-home-loving-stone-single-words']
        ),
        'control' => array(
            'label'                 =>  __( 'Number Of Words', 'charitize' ),
            'description'           =>  __( 'Give number of words you wish to be appear on home page Loving Stone section', 'charitize' ),
            'section'               => 'charitize-loving-stone-section-settings',
            'type'                  => 'number',
            'priority'              => 20,
            'active_callback'       => '',
            'input_attrs' => array( 'min' => 1, 'max' => 200),

        )
    );

/*Left button text*/
$charitize_settings_controls['charitize-home-loving-stone-button-text'] =
array(
    'setting' =>     array(
        'default'              => $charitize_customizer_defaults['charitize-home-loving-stone-button-text']
    ),
    'control' => array(
        'label'                 =>  __( 'Left Button Text', 'charitize' ),
        'description'           =>  __( 'Input text for display on button', 'charitize' ),
        'section'               => 'charitize-loving-stone-section-settings',
        'type'                  => 'text',
        'priority'              => 60,
        'active_callback'       => ''
    )
);

/*Left button url*/
$charitize_settings_controls['charitize-home-loving-stone-button-link'] =
array(
    'setting' =>     array(
        'default'              => $charitize_customizer_defaults['charitize-home-loving-stone-button-link']
    ),
    'control' => array(
        'label'                 =>  __( 'Left Button URL', 'charitize' ),
        'description'           =>  __( 'Will redirect to the specified URL ', 'charitize' ),
        'section'               => 'charitize-loving-stone-section-settings',
        'type'                  => 'url',
        'priority'              => 70,
        'active_callback'       => ''
    )
);


/*Right button text*/
// $charitize_settings_controls['charitize-home-loving-stone-right-button-text'] =
// array(
//     'setting' =>     array(
//         'default'              => $charitize_customizer_defaults['charitize-home-loving-stone-right-button-text']
//     ),
//     'control' => array(
//         'label'                 =>  __( 'Right Button Text', 'charitize' ),
//         'description'           =>  __( 'Input text for display on button', 'charitize' ),
//         'section'               => 'charitize-loving-stone-section-settings',
//         'type'                  => 'text',
//         'priority'              => 80,
//         'active_callback'       => ''
//     )
// );

/*Right button url*/
// $charitize_settings_controls['charitize-home-loving-stone-right-button-link'] =
// array(
//     'setting' =>     array(
//         'default'              => $charitize_customizer_defaults['charitize-home-loving-stone-right-button-link']
//     ),
//     'control' => array(
//         'label'                 =>  __( 'Right Button URL', 'charitize' ),
//         'description'           =>  __( 'Will redirect to the specified URL ', 'charitize' ),
//         'section'               => 'charitize-loving-stone-section-settings',
//         'type'                  => 'url',
//         'priority'              => 90,
//         'active_callback'       => ''
//     )
// );