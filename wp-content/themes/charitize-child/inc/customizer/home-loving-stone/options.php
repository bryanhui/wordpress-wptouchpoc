<?php
global $charitize_panels;
global $charitize_sections;
global $charitize_settings_controls;
global $charitize_repeated_settings_controls;
global $charitize_customizer_defaults;

/*defaults values loving stone options*/
$charitize_customizer_defaults['charitize-loving-stone-enable'] = 0;
$charitize_customizer_defaults['charitize-loving-stone-page'] = 0;

/* Feature section Enable settings*/
$charitize_sections['charitize-loving-stone-options'] =
    array(
        'priority'       => 10,
        'title'          => __( 'Section Options', 'charitize' ),
        'panel'          => 'charitize-loving-stone-panel',
    );

/*Loving Stone section enable control*/
/*Loving Stone section enable control*/
$charitize_settings_controls['charitize-loving-stone-enable'] =
    array(
        'setting' =>     array(
            'default'              => $charitize_customizer_defaults['charitize-loving-stone-enable']
        ),
        'control' => array(
            'label'                 =>  __( 'Enable Loving Stone Section', 'charitize' ),
            'description'           =>  __( 'Enable "Loving Stone Section" on checked', 'charitize' ),
            'section'               => 'charitize-loving-stone-options',
            'type'                  => 'checkbox',
            'priority'              => 10,
            'active_callback'       => ''
        )
    );


    /*creating setting control for charitize-loving-stone-page start*/
    $charitize_settings_controls['charitize-loving-stone-page'] =
        array(
                'setting' =>     array(
                    'default'              => $charitize_customizer_defaults['charitize-loving-stone-page'],
                    ),
                'control' => array(
                    'label'                 =>  __( 'Select Page For Loving Stone Section', 'charitize' ),
                    'description'           => '',
                    'section'               => 'charitize-loving-stone-options',
                    'type'                  => 'dropdown-pages',
                    'priority'              => 20
                )
        );
