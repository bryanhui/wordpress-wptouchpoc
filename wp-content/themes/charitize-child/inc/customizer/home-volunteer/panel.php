<?php
global $charitize_panels;
/*creating panel for Volunteer Section setting*/
$charitize_panels['charitize-volunteer-panel'] =
    array(
        'title'          => __( 'Home/Front Volunteer Section', 'charitize' ),
        'priority'       => 211
    );

/*volunteer section enable control */
require(dirname(__FILE__).'/options.php');
//require get_template_directory().'/inc/customizer/home-donate/options.php';

/*volunteer selection settings */
require(dirname(__FILE__).'/settings.php');
//require get_template_directory().'/inc/customizer/home-donate/settings.php';