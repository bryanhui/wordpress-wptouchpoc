<?php
global $charitize_panels;
global $charitize_sections;
global $charitize_settings_controls;
global $charitize_repeated_settings_controls;
global $charitize_customizer_defaults;

/*defaults values volunteer options*/
$charitize_customizer_defaults['charitize-home-volunteer-single-words'] = 40;
$charitize_customizer_defaults['charitize-home-volunteer-button-text'] = __('Find Out More', 'charitize-child');
$charitize_customizer_defaults['charitize-home-volunteer-button-link'] = '';

/* Feature section Enable settings*/
$charitize_sections['charitize-volunteer-section-settings'] =
    array(
        'priority'       => 30,
        'title'          => __( 'Section Settings', 'charitize' ),
        'panel'          => 'charitize-volunteer-panel',
    );


/*String in max to be appear as description on volunteer*/
$charitize_settings_controls['charitize-home-volunteer-single-words'] =
    array(
        'setting' =>     array(
            'default'              => $charitize_customizer_defaults['charitize-home-volunteer-single-words']
        ),
        'control' => array(
            'label'                 =>  __( 'Number Of Words', 'charitize' ),
            'description'           =>  __( 'Give number of words you wish to be appear on home page volunteer section', 'charitize' ),
            'section'               => 'charitize-volunteer-section-settings',
            'type'                  => 'number',
            'priority'              => 20,
            'active_callback'       => '',
            'input_attrs' => array( 'min' => 1, 'max' => 200),

        )
    );

/*Volunteer button text*/
$charitize_settings_controls['charitize-home-volunteer-button-text'] =
array(
    'setting' =>     array(
        'default'              => $charitize_customizer_defaults['charitize-home-volunteer-button-text']
    ),
    'control' => array(
        'label'                 =>  __( 'Button Text', 'charitize' ),
        'description'           =>  __( 'Input text for display on button', 'charitize' ),
        'section'               => 'charitize-home-volunteer-button-text',
        'type'                  => 'text',
        'priority'              => 60,
        'active_callback'       => ''
    )
);

/*Volunteer button url*/
$charitize_settings_controls['charitize-home-volunteer-button-link'] =
array(
    'setting' =>     array(
        'default'              => $charitize_customizer_defaults['charitize-home-volunteer-button-link']
    ),
    'control' => array(
        'label'                 =>  __( 'Volunteer Section Button URL', 'charitize' ),
        'description'           =>  __( 'Will redirect to the specified URL ', 'charitize' ),
        'section'               => 'charitize-volunteer-section-settings',
        'type'                  => 'url',
        'priority'              => 70,
        'active_callback'       => ''
    )
);
