<?php
global $charitize_panels;
global $charitize_sections;
global $charitize_settings_controls;
global $charitize_repeated_settings_controls;
global $charitize_customizer_defaults;

/*defaults values volunteer options*/
$charitize_customizer_defaults['charitize-volunteer-enable'] = 0;
$charitize_customizer_defaults['charitize-volunteer-page'] = 0;

/* Feature section Enable settings*/
$charitize_sections['charitize-volunteer-options'] =
    array(
        'priority'       => 10,
        'title'          => __( 'Section Options', 'charitize' ),
        'panel'          => 'charitize-volunteer-panel',
    );

/*Volunteer section enable control*/
/*Volunteer section enable control*/
$charitize_settings_controls['charitize-volunteer-enable'] =
    array(
        'setting' =>     array(
            'default'              => $charitize_customizer_defaults['charitize-volunteer-enable']
        ),
        'control' => array(
            'label'                 =>  __( 'Enable Volunteer Section', 'charitize' ),
            'description'           =>  __( 'Enable "Volunteer Section" on checked', 'charitize' ),
            'section'               => 'charitize-volunteer-options',
            'type'                  => 'checkbox',
            'priority'              => 10,
            'active_callback'       => ''
        )
    );


    /*creating setting control for charitize-volunteer-page start*/
    $charitize_settings_controls['charitize-volunteer-page'] =
        array(
                'setting' =>     array(
                    'default'              => $charitize_customizer_defaults['charitize-volunteer-page'],
                    ),
                'control' => array(
                    'label'                 =>  __( 'Select Page For Volunteer Section', 'charitize' ),
                    'description'           => '',
                    'section'               => 'charitize-volunteer-options',
                    'type'                  => 'dropdown-pages',
                    'priority'              => 20
                )
        );
