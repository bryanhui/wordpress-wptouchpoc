<?php

if ( ! function_exists( 'charitize_header' ) ) :
    /**
     * Main header
     *
     * @since charitize 1.0.0
     *
     * @param null
     * @return null
     *
     */
    function charitize_header() {
        global $charitize_customizer_all_values;
        global $wp_version;
        ?>

        <div class="section">
            <div class="background-image" style="background-image : url('https://static.wixstatic.com/media/40169d_dd5fd3612635497ba3103259257a690a.jpg/v1/fill/w_1920,h_1370,al_c,q_85,usm_0.66_1.00_0.01/40169d_dd5fd3612635497ba3103259257a690a.webp')"></div>
            <div class="container">
                <div class="row">
                    <div class="col-md-12 text-center site-logo-col">
                        <?php charitize_the_custom_logo();?>
                    </div>
                </div>
            </div>
        </div>
        <div class="navbar navbar-default navbar-static-top nav-bar-style nav-menu">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-ex-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand"></a>
                </div>
                <div class="collapse navbar-collapse main-nav-text-center" id="navbar-ex-collapse">
                    <?php
                        wp_nav_menu( array(
                            'theme_location' => 'primary',
                            'depth' => 2,
                            'container' => false,
                            'menu_class'     => 'nav navbar-nav',
                            'fallback_cb' => 'WP_Bootstrap_Navwalker::fallback',
                            'walker' => new WP_Bootstrap_Navwalker(),
                        ) );
                    ?>
                    <?php
                        wp_nav_menu( array(
                            'theme_location' => 'language',
                            'depth' => 2,
                            'container' => false,
                            'menu_class'     => 'nav navbar-nav pull-right',
                            'fallback_cb' => 'WP_Bootstrap_Navwalker::fallback',
                            'walker' => new WP_Bootstrap_Navwalker(),
                        ) );
                    ?>
                </div>
            </div>
        </div>
    <?php
    }
endif;
add_action( 'charitize_action_header', 'charitize_header', 10 );

