<?php

// Configuration common to all environments
include_once __DIR__ . '/wp-config.common.php';

/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wptouchpoc');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '2lOVExPRESS1');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '9*ZsDKIMS]Ni;y`y%{]F;L|sc8uU^:/%Go|!pb{t-FfA9D2TKZBA]%@_s,auwoI6');
define('SECURE_AUTH_KEY',  'C_f1=*s<!/^vviJgEx$Cm!tQiHj$e.HVUBH$45GnZRJi(SfDkPI-h8-#PE-{Y>~~');
define('LOGGED_IN_KEY',    'j?6<`FOq@m%a6~PvO}V]I?`h:0*hmOrIv,Tohqly}tXZLUhgX}BE]5GVs/im$/SV');
define('NONCE_KEY',        '~yB:*]F,ve$H>_a,,,IV?22gRtG[`@?i`-6g8%^=HY46C6)`cX}~_*Pl>Q~>#i3u');
define('AUTH_SALT',        'uReTjrPaD?eV))gk~D]PH.7#>}C>fuuC w|R4UTiP+~2YL0W~>gO;R4-[^H>m=Rj');
define('SECURE_AUTH_SALT', 't]awD<jl-(5%>BrLW!~K}%wK9l~fk%}QBd@sD7.GCm=.CNUFF;O9KM_fEX]iqa4P');
define('LOGGED_IN_SALT',   'rFc;3OE4x3R?OWvh%#cstP@=cVBj:6A]FdzBGahlV9_u/BTv)qQcW8<q9:c~(4yq');
define('NONCE_SALT',       '$:sXG2;6HH#!^Ll0~@P,]Y.2qgo?PV99dJ+95+BxgG}34-!YA}UjGAja=>?lRdzO');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wptouchpoc';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

define('VP_ENVIRONMENT', 'default');
/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
if(is_admin()) {
   add_filter('filesystem_method', create_function('$a', 'return "direct";' ));
   define( 'FS_CHMOD_DIR', 0751 );
}
